import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirstApp());
}

class MyFirstApp extends StatelessWidget {
  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PSU TRANG',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.account_circle_rounded),
          title: Text('My First App'),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.add),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.search),
            ),
          ],
        ),
        body: Center(
          child: Column(
            children: [
              CircleAvatar(
                backgroundImage: AssetImage('assets/chicken.jpg'),
                radius: 250,
              ),
              // Image.asset('assets/chicken.jpg'
              // ,width: 600,height: 400,),
              Text(
                  'My name is Chicken KFC.',
              style: TextStyle(
                height: 1,
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// class MyFirstApp extends StatelessWidget {
//   const MyFirstApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home:
//     );
//   }
// }
